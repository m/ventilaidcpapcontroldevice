/*
 * LED.c
 *
 *  Created on: 01.04.2020
 *      Author: Maciej
 */

#include "LED.h"
#include "spi.h"

void LedInit(void)
{
    //TODO: call SPI2 init function from here
}

void LedSetRgb(uint8_t _R, uint8_t _G, uint8_t _B)
{
    uint32_t input_value = _G;
    input_value = input_value << 8;
    input_value |= _R;
    input_value = input_value << 8;
    input_value |= _B;
    uint32_t encoded_value = 0;
    uint8_t buffer[10] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    //green
    for (uint8_t i = 0; i < 8; i++) {
        encoded_value = encoded_value << 3;
        if (0 == (input_value & (1 << (23 - i)))) {
            encoded_value |= LED_LOGIC_0;
        } else {
            encoded_value |= LED_LOGIC_1;
        }
    }
    buffer[0] = ((encoded_value >> 16) & 0xff);
    buffer[1] = ((encoded_value >> 8) & 0xff);
    buffer[2] = (encoded_value & 0xff);

    //red
    encoded_value = 0;
    for (uint8_t i = 0; i < 8; i++) {
        encoded_value = encoded_value << 3;
        if (0 == (input_value & (1 << (15 - i)))) {
            encoded_value |= LED_LOGIC_0;
        } else {
            encoded_value |= LED_LOGIC_1;
        }
    }
    buffer[3] = ((encoded_value >> 16) & 0xff);
    buffer[4] = ((encoded_value >> 8) & 0xff);
    buffer[5] = (encoded_value & 0xff);

    //blue
    encoded_value = 0;
    for (uint8_t i = 0; i < 8; i++) {
        encoded_value = encoded_value << 3;
        if (0 == (input_value & (1 << (7 - i)))) {
            encoded_value |= LED_LOGIC_0;
        } else {
            encoded_value |= LED_LOGIC_1;
        }
    }
    buffer[6] = ((encoded_value >> 16) & 0xff);
    buffer[7] = ((encoded_value >> 8) & 0xff);
    buffer[8] = (encoded_value & 0xFF);
    buffer[9] = 0; //to ensure MOSI idle LOW

    HAL_SPI_Transmit(&hspi2, buffer, 10, 0xFFFF);
}
