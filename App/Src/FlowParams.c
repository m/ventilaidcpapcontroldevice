/*
 * FlowParams.c
 *
 *  Created on: 03.06.2020
 *      Author: Maciej
 */

#include "FlowParams.h"
#include "Eeprom.h"
#include "EepromMemoryMap.h"

void FlowParamsLoadFromEeprom(void)
{
    EepromReadValue(EEPROM_ADDR_FLOW_C0, &Flow_calibration_C0);
    EepromReadValue(EEPROM_ADDR_FLOW_C1, &Flow_calibration_C1);
    EepromReadValue(EEPROM_ADDR_FLOW_C2, &Flow_calibration_C2);
    EepromReadValue(EEPROM_ADDR_FLOW_C3, &Flow_calibration_C3);
    EepromReadValue(EEPROM_ADDR_FLOW_C4, &Flow_calibration_C4);
}

void FlowParamsStoreReceivedParams(void)
{

    Flow_calibration_C0 = Flow_calibration_set_C0;
    Flow_calibration_C1 = Flow_calibration_set_C1;
    Flow_calibration_C2 = Flow_calibration_set_C2;
    Flow_calibration_C3 = Flow_calibration_set_C3;
    Flow_calibration_C4 = Flow_calibration_set_C4;
}
