/*
 * Ventilator.c
 *
 *  Created on: 01.04.2020
 *      Author: Maciej
 */

#include "Ventilator.h"
#include "Breathalyzer.h"
#include "Buzzer.h"
#include "FlowEstimate.h"
#include "LED.h"
#include "PIDParams.h"
#include "Peep.h"
#include "PressureSensor.h"
#include "RpmSensor.h"
#include "math.h"
#include <PidController.h>
#include <RcPwmOutput.h>
extern float RPMAvg;
float PressBand = 1;
extern uint8_t InitDone;
extern float FlowEps;
extern uint8_t RPMReached;

uint8_t CurrentPhase = dOther;
uint8_t PreviousPhase = dOther;
uint32_t LastOfType[9];

extern PIDControl* pid;
extern PIDControl* PeepPid;
uint8_t MotorStarted = 0;
uint16_t MotorOutput = 0;
uint16_t output = 0;

uint32_t iii = 0;
extern float PressPAvg;
extern float PressFAvg;

extern float PressAvg[4];

extern float Press0Sum;
extern float Press1Sum;
extern float Press2Sum;
extern float Press3Sum;
extern float Press0Avg;
extern float Press1Avg;
extern float Press2Avg;
extern float Press3Avg;
float uchyb = 0;
float peep = 0;
extern float Flow;

extern uint16_t PressMaxSample;
extern uint16_t PressBuffIndex;
extern uint8_t PressBuffReady;
uint16_t counter = 0;

void ChangeSetpoint()
{
    PIDSetpointSet(pid, 8);

    /*if(PotentiometerReadScaled(4)<20){
        PIDSetpointSet(pid, 0);
    }else if(PotentiometerReadScaled(4)<50){
        PIDSetpointSet(pid, ventilator_medical_PEEP);
    }else if(PotentiometerReadScaled(4)<80){*/
    iii++;
    if (iii < defTimInh) {
        PIDSetpointSet(pid, ventilator_medical_PIP);
        PIDSetpointSet(PeepPid, ventilator_medical_PIP);
        //BuzzerSetOutput(1);
        //RcPwmMotorSetOutput(160);
        LedSetRgb(0, 100, 100);

    } else if (iii < defTimExh) {
        PIDSetpointSet(pid, ventilator_medical_PEEP);
        PIDSetpointSet(PeepPid, ventilator_medical_PEEP);
        //BuzzerSetOutput(0);
        //RcPwmMotorSetOutput(1000);
        LedSetRgb(0, 100, 255);

    } else
        iii = 0;
    /*} else{
        if((dInhale==BreathBuff[BreathBuffIndex].State)&&(0==BreathBuff[BreathBuffIndex].ForceExhale)){
            PIDSetpointSet(pid, ventilator_medical_PIP);
        } else{
             PIDSetpointSet(pid, ventilator_medical_PEEP);
        }
    }*/
}

void PIDUpdate()
{
    uchyb = (pid->setpoint - PressPAvg) / pid->setpoint;
    if ((pid->setpoint - PressBand) > PressPAvg) {
        PIDTuningsChoseGroup(pid, 0);
    } else if ((pid->setpoint + PressBand) < PressPAvg) {
        PIDTuningsChoseGroup(pid, 2);
    } else {
        PIDTuningsChoseGroup(pid, 1);
    }
    PIDInputSet(pid, PressPAvg);
    PIDCompute(pid);

    //MotorOutput = (uint16_t)pid->output;
    MotorOutput = (uint16_t)pid->smoothedOutput;
    PIDInputSet(PeepPid, PressPAvg);

    //    if(uchyb<0.2){
    //        PeepSetOutput((uint16_t) uchyb*PeepPid->dispKp);
    //    }
    //    else {
    //        PeepSetOutput((uint16_t) 0);//
    //
    //    }
    PIDCompute(PeepPid);
    //PeepSetOutput((uint16_t)PeepPid->smoothedOutput);
    //output = (ventilator_medical_PEEP - 5) * 100;
    //MotorOutput = output;
}

void VentilatorInit(void)
{
    ventilator_medical_PIP = 5;
    ventilator_medical_PEEP = 5;
    ventilator_medical_BreathPerMin = 10;
    ventilator_medical_IEratio = 3;
    ventilator_medical_TargetVol = 4;

    defTimInh = 600;
    defTimExh = 1200;
    defTimBre = defTimInh + defTimExh;

    MotorTemp = 0;

    CreateBreath();
}

/*
 * Main calculation function, useful functions/variables:
 * 1. ventilator_set_* and ventilator_current_* variables (ventilator.h),
 * 2. PressureSensorReadPressure(_sensor_number) [number of pressure sensors
 * should be declared in h file] (sensor_number 1->max_sensor_no)
 * (PressureSensor.h),
 * 3. MotorSetOutput(_output_level), value should be [0-100] (MotorOutput.h),
 * 4. LEDSetRGB(R,G,B), values should be [0-1] (LED.h).
 * 5. PotentiometerReadRaw()[returns 0-4030] or
 * PotentiometerReadScaled()[returns 0-100](adc.h)
 * 6. Voltage5VReadValue() and Voltage12VReadValue() (adc.h);
 * 7. PeepSetOutput(0-1) (Peep.h)
 * 8. BuzzerSetOutput(0-1) (Buzzer.h)
 * Frequency can be changed in main.h file
 */

void VentilatorSpin(void)
{
    // Calculate averages
    RPMAverage();
    PressAverage();
    // Estimate Flow
    FlowEstimate();
    // Analyze, detect phase
    CalUpdate();
    Breathalizer();
    // If new phase, change setpoint
    RPMIsReached(MotorOutput);
    //    if (0 == RPMReached) {
    //        MotorTemp++;
    //    } else
    //        MotorTemp = 0;
    if (1 == InitDone) {
        ChangeSetpoint();
        // Calculate Motor PID
        PIDUpdate();
        RcPwmMotorSetOutput(MotorOutput);
    }
    ventilator_current_pressure = PressPAvg;
    ventilator_current_flow = Flow;
    Pressure_Sensor_0 = PressAvg[0];
    Pressure_Sensor_1 = PressAvg[1];
    Pressure_Sensor_2 = PressAvg[2];
    Pressure_Sensor_3 = PressAvg[3];
    //MotorOut = pid->smoothedOutput;
    MotorOut = MotorOutput;
    MotorRPM = RPMAvg;
    MotorCurr = 0;
    //MotorTemp = 0;
    ventilator_DeviceTemp = 0;
    ventilator_AmbientTemp = 0;
    ventilator_AmbientHumidity = 0;
    ventilator_PEEPOut = 0;
    ventilator_current_inhVol = ventilator_medical_TargetVol;
    //ventilator_current_PEEP = ventilator_medical_set_PEEP;
    //ventilator_current_PIP = ventilator_medical_set_PIP;

    // Calculate PEEP PID
    // Record state, calculate averages
    // Analyze Alarms

    //RcPwmServo0SetOutput((ventilator_medical_PEEP-5)*100);
    RcPwmServo0SetOutput(PID_PEEPPID_P);
    MotorTemp = PID_PEEPPID_P;
}

void VentilatorStoreReceivedMedicalSettings(void)
{
    ventilator_medical_PEEP = ventilator_medical_set_PEEP;
    ventilator_medical_TargetFiO2 = ventilator_medical_set_TargetFiO2;
    ventilator_medical_PIP = ventilator_medical_set_PIP;
    ventilator_medical_IEratio = ventilator_medical_set_IEratio;
    ventilator_medical_BreathPerMin = ventilator_medical_set_BreathPerMin;
    ventilator_medical_TargetVol = ventilator_medical_set_TargetVol;
    ventilator_medical_Mode = ventilator_medical_set_Mode;
}
