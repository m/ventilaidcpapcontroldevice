/*
 * FlowEstimate.c
 *
 *  Created on: May 18, 2020
 *      Author: ventilaid
 */

#include "main.h"
#include "math.h"

#define FlowConverter 50.1

extern float PressPAvg;
extern float PressFAvg;
extern float PressFAvgRaw;
float Flow = 0;

float FlowSumIn = 0;
float FlowSumEx = 0;

float FlowSumInRaw = 0;
float FlowSumExRaw = 0;

float FlowEps = 1.0;

float X1 = 0;
float X3 = 0;
float X5 = 0;

float FlowBuff[dSamplesPerMinute / 10];
uint32_t FlowBuffIndex = 0;
uint32_t FlowBuffMax = dSamplesPerMinute / 10;
uint8_t FlowBuffReady = 0;

float RawCenter = 8196;
float C0 = 4;
float C1 = 5.22330;
float C3 = 0.04563;
float C5 = 0.00006;

void FlowEstimate(void)
{
    //After rewriting cbrt to int32, all values can be integer.
    X1 = cbrt(PressFAvgRaw - RawCenter);
    X3 = X1 * X1 * X1;
    X5 = X3 * X1 * X1;
    Flow = C0 + X1 * C1 + X3 * C3 + X5 * C5;
}
