/*
 * WorkpointParams.c
 *
 *  Created on: 03.06.2020
 *      Author: Maciej
 */

#include "WorkpointParams.h"
#include "Eeprom.h"
#include "EepromMemoryMap.h"

void WorkpointParamsLoadFromEeprom(void)
{
    EepromReadValue(EEPROM_ADDR_WORKPOINT_C0, &Workpoint_calibration_C0);
    EepromReadValue(EEPROM_ADDR_WORKPOINT_C1, &Workpoint_calibration_C1);
    EepromReadValue(EEPROM_ADDR_WORKPOINT_C2, &Workpoint_calibration_C2);
    EepromReadValue(EEPROM_ADDR_WORKPOINT_C3, &Workpoint_calibration_C3);
    EepromReadValue(EEPROM_ADDR_WORKPOINT_C4, &Workpoint_calibration_C4);
}

void WorkpointParamsStoreReceivedParams(void)
{

    Workpoint_calibration_C0 = Workpoint_calibration_set_C0;
    Workpoint_calibration_C1 = Workpoint_calibration_set_C1;
    Workpoint_calibration_C2 = Workpoint_calibration_set_C2;
    Workpoint_calibration_C3 = Workpoint_calibration_set_C3;
    Workpoint_calibration_C4 = Workpoint_calibration_set_C4;
}
