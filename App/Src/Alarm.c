/*
 * Alarm.c
 *
 *  Created on: 26 May 2020
 *      Author: Piotr
 */

#include "Alarm.h"
#include "App.h"

/* BEGIN: Variables for checking alarm conditions */
// TODO: Set in correct place
float voltage_value = 5.0;

float battery_voltage_value = 4.1;

float oxygen_pressure = 720.0;
float oxygen_concentration = 20.0;

float air_pressure = 1.2;

float air_volume_minute_total = 50.0;
float air_volume_breath = 50.0;

uint8_t hoses_leak = FALSE;
uint8_t hoses_disconnected = FALSE;

uint8_t breaths_per_minute = 18;

uint8_t motor_alarm = FALSE;
uint8_t pressure_sensor_alarm = FALSE;
uint8_t user_panel_alarm = FALSE;
/* END: Variables for checking alarm conditions */

void AlarmInit(void)
{
    current_status = INITIALIZING;
    Alarm_code = (ALARM_DEVICE_STATUS_MASK & current_status);

    AlarmPowerSupplyInit();
    AlarmBatterySupplyInit();
    AlarmOxygenInit();
    AlarmAirPressureInit();
    AlarmAirVolumeInit();
    AlarmHosesInit();
    AlarmBreathsPerMinuteInit();
    AlarmDeviceInit();
}

void AlarmSetAlarm(tAlarm Alarm, uint8_t detail_bit)
{
    Alarm_code_detail = (1 << Alarm.bit_num);
    Alarm_code |= Alarm_code_detail;
    Alarm.detail_bit = detail_bit;

    AlarmGetDetails(Alarm);
    AppSendAlarmDetailsMessage();
}

void AlarmGetDetails(tAlarm Alarm)
{
    Alarm_detail = (1 << Alarm.detail_bit);
}

void AlarmClearAlarm(tAlarm Alarm)
{
    Alarm_code &= ~(1 << Alarm.bit_num);
    Alarm.detail_bit = ALARM_NO_ALARMS_BIT;
}

void AlarmPowerSupplyInit(void)
{
    Alarm_power_supply.bit_num = ALARM_POWER_SUPPLY_BIT;
    Alarm_power_supply.detail_bit = POWER_SUPPLY_NO_ALARMS;
}

void AlarmPowerSupplyCheck(void)
{
    if (voltage_value == 0)
        AlarmSetAlarm(Alarm_power_supply, POWER_SUPPLY_NO_VOLTAGE);
    else if (voltage_value < POWER_SUPPLY_MIN_VOLTAGE)
        AlarmSetAlarm(Alarm_power_supply, POWER_SUPPLY_VOLTAGE_TOO_LOW);
    else if (voltage_value > POWER_SUPPLY_MAX_VOLTAGE)
        AlarmSetAlarm(Alarm_power_supply, POWER_SUPPLY_VOLTAGE_TOO_HIGH);
    else
        AlarmClearAlarm(Alarm_power_supply);
}

void AlarmBatterySupplyInit(void)
{
    Alarm_battery_supply.bit_num = ALARM_BATTERY_SUPPLY_BIT;
    Alarm_battery_supply.detail_bit = BATTERY_SUPPLY_NO_ALARMS;
}

void AlarmBatterySupplyCheck(void)
{
    if (battery_voltage_value == 0)
        AlarmSetAlarm(Alarm_battery_supply, BATTERY_SUPPLY_NO_BATTERY_CONNECTED);
    else if (battery_voltage_value < POWER_SUPPLY_MIN_VOLTAGE)
        AlarmSetAlarm(Alarm_battery_supply, BATTERY_SUPPLY_VOLTAGE_TOO_LOW);
    else if (battery_voltage_value > POWER_SUPPLY_MAX_VOLTAGE)
        AlarmSetAlarm(Alarm_battery_supply, BATTERY_SUPPLY_VOLTAGE_TOO_HIGH);
    else
        AlarmClearAlarm(Alarm_battery_supply);
}

void AlarmOxygenInit(void)
{
    Alarm_oxygen.bit_num = ALARM_OXYGEN_BIT;
    Alarm_oxygen.detail_bit = OXYGEN_NO_ALARMS;
}

void AlarmOxygenCheck(void)
{
    uint8_t oxygen_alarm_was_set = FALSE;

    if (oxygen_pressure < OXYGEN_PRESSURE_MIN_VALUE) {
        AlarmSetAlarm(Alarm_oxygen, OXYGEN_PRESSURE_TOO_LOW);
        oxygen_alarm_was_set = TRUE;
    } else if (oxygen_pressure > OXYGEN_PRESSURE_MAX_VALUE) {
        AlarmSetAlarm(Alarm_oxygen, OXYGEN_PRESSURE_TOO_HIGH);
        oxygen_alarm_was_set = TRUE;
    }

    if (oxygen_concentration < OXYGEN_CONCENTRATION_MIN_VALUE) {
        AlarmSetAlarm(Alarm_oxygen, OXYGEN_CONCENTRATION_TOO_LOW);
        oxygen_alarm_was_set = TRUE;
    } else if (oxygen_concentration > OXYGEN_CONCENTRATION_MAX_VALUE) {
        AlarmSetAlarm(Alarm_oxygen, OXYGEN_CONCENTRATION_TOO_HIGH);
        oxygen_alarm_was_set = TRUE;
    }

    if (oxygen_alarm_was_set == FALSE)
        AlarmClearAlarm(Alarm_oxygen);
}

void AlarmAirPressureInit(void)
{
    Alarm_air_pressure.bit_num = ALARM_AIR_PRESSURE_BIT;
    Alarm_air_pressure.detail_bit = AIR_PRESSURE_NO_ALARMS;
}

void AlarmAirPressureCheck(void)
{
    if (air_pressure < AIR_PRESSURE_MIN_VALUE)
        AlarmSetAlarm(Alarm_air_pressure, AIR_PRESSURE_TOO_LOW);
    else if (air_pressure > AIR_PRESSURE_MAX_VALUE)
        AlarmSetAlarm(Alarm_air_pressure, AIR_PRESSURE_TOO_HIGH);
    else
        AlarmClearAlarm(Alarm_air_pressure);
}

void AlarmAirVolumeInit(void)
{
    Alarm_air_volume.bit_num = ALARM_AIR_VOLUME_BIT;
    Alarm_air_volume.detail_bit = AIR_VOLUME_NO_ALARMS;
}

void AlarmAirVolumeCheck(void)
{
    uint8_t air_volume_alarm_was_set = FALSE;

    if (air_volume_minute_total < AIR_VOLUME_MINUTE_TOTAL_MIN_VALUE) {
        AlarmSetAlarm(Alarm_air_volume, AIR_VOLUME_MINUTE_TOTAL_TOO_LOW);
        air_volume_alarm_was_set = TRUE;
    } else if (air_volume_minute_total > AIR_VOLUME_MINUTE_TOTAL_MAX_VALUE) {
        AlarmSetAlarm(Alarm_air_volume, AIR_VOLUME_MINUTE_TOTAL_TOO_HIGH);
        air_volume_alarm_was_set = TRUE;
    }

    if (air_volume_breath < AIR_VOLUME_BREATH_VOLUME_MIN_VALUE) {
        AlarmSetAlarm(Alarm_air_volume, AIR_VOLUME_BREATH_VOLUME_TOO_LOW);
        air_volume_alarm_was_set = TRUE;
    } else if (air_volume_breath > AIR_VOLUME_BREATH_VOLUME_MAX_VALUE) {
        AlarmSetAlarm(Alarm_air_volume, AIR_VOLUME_BREATH_VOLUME_TOO_HIGH);
        air_volume_alarm_was_set = TRUE;
    }

    if (air_volume_alarm_was_set == FALSE)
        AlarmClearAlarm(Alarm_air_volume);
}

void AlarmHosesInit(void)
{
    Alarm_hoses.bit_num = ALARM_HOSES_BIT;
    Alarm_hoses.detail_bit = HOSES_NO_ALARMS;
}

void AlarmHosesCheck(void)
{
    if (hoses_disconnected != FALSE)
        AlarmSetAlarm(Alarm_hoses, HOSES_DISCONNECTED);
    else if (hoses_leak != FALSE)
        AlarmSetAlarm(Alarm_hoses, HOSES_LEAK);
    else
        AlarmClearAlarm(Alarm_hoses);
}

void AlarmBreathsPerMinuteInit(void)
{
    Alarm_breaths_per_minute.bit_num = ALARM_BREATHS_PER_MINUTE_BIT;
    Alarm_breaths_per_minute.detail_bit = BREATHS_PER_MINUTE_NO_ALARMS;
}

void AlarmBreathsPerMinuteCheck(void)
{
    if (breaths_per_minute == 0)
        AlarmSetAlarm(Alarm_breaths_per_minute, BREATHS_PER_MINUTE_NO_BREATHS);
    else if (breaths_per_minute < BREATHS_PER_MINUTE_MIN_VALUE)
        AlarmSetAlarm(Alarm_breaths_per_minute, BREATHS_PER_MINUTE_TOO_LOW);
    else if (breaths_per_minute > BREATHS_PER_MINUTE_MAX_VALUE)
        AlarmSetAlarm(Alarm_breaths_per_minute, BREATHS_PER_MINUTE_TOO_HIGH);
    else
        AlarmClearAlarm(Alarm_breaths_per_minute);
}

void AlarmDeviceInit(void)
{
    Alarm_device.bit_num = ALARM_DEVICE_BIT;
    Alarm_device.detail_bit = DEVICE_NO_ALARMS;
}

void AlarmDeviceCheck(void)
{
    uint8_t device_alarm_was_set = FALSE;

    if (motor_alarm != FALSE) {
        AlarmSetAlarm(Alarm_device, DEVICE_MOTOR_ALARM);
        device_alarm_was_set = TRUE;
    }

    if (pressure_sensor_alarm != FALSE) {
        AlarmSetAlarm(Alarm_device, DEVICE_PRESSURE_SENSOR_ALARM);
        device_alarm_was_set = TRUE;
    }

    if (user_panel_alarm != FALSE) {
        AlarmSetAlarm(Alarm_device, DEVICE_USER_PANEL_ALARM);
        device_alarm_was_set = TRUE;
    }

    if (device_alarm_was_set == FALSE)
        AlarmClearAlarm(Alarm_device);
}

void AlarmCheckAlarmsAndUpdateDeviceStatus(void)
{
    static uint32_t prev_Alarm_code = 0;

    AlarmPowerSupplyCheck();
    AlarmBatterySupplyCheck();
    AlarmOxygenCheck();
    AlarmAirPressureCheck();
    AlarmAirVolumeCheck();
    AlarmHosesCheck();
    AlarmBreathsPerMinuteCheck();
    AlarmDeviceCheck();

    if (Alarm_code != prev_Alarm_code) {
        if (!(Alarm_code & ~(ALARM_DEVICE_STATUS_MASK))) {
            current_status = RUNNING;
        } else if (Alarm_code & ALARM_DEVICE_MASK)
            current_status = DEVICE_FAILURE;
        else
            current_status = ALARM;

        Alarm_code = ((~(ALARM_DEVICE_STATUS_MASK)&Alarm_code) | (ALARM_DEVICE_STATUS_MASK & current_status)); // Update device status in Alarm_code variable

        AppSendAlarmStatusMessage();
    }
}
