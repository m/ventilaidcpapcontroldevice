/*
 * Ventilator.h
 *
 *  Created on: 01.04.2020
 *      Author: Maciej
 */

#ifndef INC_VENTILATOR_H_
#define INC_VENTILATOR_H_

#include "main.h"

uint32_t defTimInh;
uint32_t defTimExh;
uint32_t defTimBre;

/* PROTOCOL VERSION V.2 */
float ventilator_current_pressure;
float ventilator_current_flow;
float ventilator_current_inhPresMean;
float ventilator_current_exhPresMean;
float ventilator_current_inhVol;
float ventilator_current_inhVolPerMin;
float ventilator_current_ExhVol;
float ventilator_current_BreathPerMin;
float ventilator_current_LungCompl;
float ventilator_current_RotTarget;
uint8_t ventilator_current_LstInhTyp;
uint8_t ventilator_current_BreathPhase;

float ventilator_medical_PEEP;
float ventilator_medical_TargetFiO2;
float ventilator_medical_PIP;
float ventilator_medical_IEratio;
float ventilator_medical_BreathPerMin;
float ventilator_medical_TargetVol;
uint8_t ventilator_medical_Mode;

float ventilator_DeviceTemp;
float ventilator_AmbientTemp;
float ventilator_AmbientHumidity;
float ventilator_PEEPOut;

//values from additional setting message

/* PROTOCOL VERSION V.2 */
uint8_t ventilator_mess_no;

float ventilator_medical_set_PEEP;
float ventilator_medical_set_TargetFiO2;
float ventilator_medical_set_PIP;
float ventilator_medical_set_IEratio;
float ventilator_medical_set_BreathPerMin;
float ventilator_medical_set_TargetVol;
uint8_t ventilator_medical_set_Mode;

void VentilatorInit(void);
/*
 * main calculation function, called every INTERRUPT_INTERVAL us (microseconds)
 */
void VentilatorSpin(void);
void VentilatorStoreReceivedMedicalSettings(void);

#endif /* INC_VENTILATOR_H_ */
