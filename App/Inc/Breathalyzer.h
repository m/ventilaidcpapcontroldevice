/*
 * Breathalyzer.h
 *
 *  Created on: May 18, 2020
 *      Author: ventilaid
 */

#ifndef INC_BREATHALYZER_H_
#define INC_BREATHALYZER_H_
#include "main.h"

void CreateBreath();
void Breathalizer();
void CalUpdate();

/* PROTOCOL VERSION V.2 */
float Breathalyser_Ptrig;
float Breathalyser_Vtrig;
float Breathalyser_Esens;
float Breathalyser_IntegralSens;

float Breathalyser_set_Ptrig;
float Breathalyser_set_Vtrig;
float Breathalyser_set_Esens;
float Breathalyser_set_IntegralSens;

typedef struct {
    uint32_t TiInhaleStart;
    uint32_t TiInhaleEnd;
    uint32_t TiExhStart;
    uint32_t TiExhEnd;
    uint32_t TiPreviousInhlae;
    uint32_t TiExhForce;
    uint32_t ExhaleWaitingTime;
    float FlowIntegralInhale;
    float FlowIntegralExhale;
    float FlowPeakInhale;
    float FlowPeakExhale;
    float PressMinInhale;
    float PressMinExhale;
    float PressMeaInhale;
    float PressMeaExhale;
    float PressMaxInhale;
    float PressMaxExhale;
    uint32_t iii;
    uint8_t TrigInSt;
    uint8_t TrigInEn;
    uint8_t TrigExSt;
    uint8_t TrigExEn;
    uint8_t State;
    uint8_t ForceExhale;
    uint8_t GotExhale;
} Breath;

Breath BreathBuff[16];

void BreathalizerStoreReceivedParams(void);

#endif /* INC_BREATHALYZER_H_ */
