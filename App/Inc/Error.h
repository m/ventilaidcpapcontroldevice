/*
 * Error.h
 *
 *  Created on: 01.04.2020
 *      Author: Maciej
 */

#ifndef INC_ERROR_H_
#define INC_ERROR_H_

#include "main.h"

void ErrorSendMessage(uint8_t _err_code);

#endif /* INC_ERROR_H_ */
