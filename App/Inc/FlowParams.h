/*
 * FlowSettings.h
 *
 *  Created on: 21 May 2020
 *      Author: Piotr
 */

#ifndef INC_FLOWPARAMS_H_
#define INC_FLOWPARAMS_H_

/* PROTOCOL VERSION V.2 */
// values that are loaded from memory on device init
float Flow_calibration_C0;
float Flow_calibration_C1;
float Flow_calibration_C2;
float Flow_calibration_C3;
float Flow_calibration_C4;

// values that are received from front panel and stored in memory
float Flow_calibration_set_C0;
float Flow_calibration_set_C1;
float Flow_calibration_set_C2;
float Flow_calibration_set_C3;
float Flow_calibration_set_C4;

void FlowParamsLoadFromEeprom(void);
void FlowParamsStoreReceivedParams(void);

#endif /* INC_FLOWPARAMS_H_ */
