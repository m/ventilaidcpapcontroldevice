/*
 * Utils.h
 *
 *  Created on: 06.04.2020
 *      Author: Maciej
 */

#ifndef INC_UTILS_H_
#define INC_UTILS_H_

#include "main.h"

typedef enum { iso_3309,
    itu_v41 } checksum_type;

static const uint16_t crc_tbl[16] = { 0x0000, 0x1081, 0x2102, 0x3183, 0x4204, 0x5285, 0x6306, 0x7387, 0x8408, 0x9489, 0xa50a, 0xb58b, 0xc60c, 0xd68d, 0xe70e, 0xf78f };

void ConvertFloatToUintArray(float _input, uint8_t* _output);
float ConvertUintArrayToFloat(uint8_t* _input);
float ConvertWordArrayToFloat(uint16_t* _input_array);
void ConvertFloatToWordArray(float _input, uint16_t* _output);
uint16_t checksum(const uint8_t* data, uint16_t len, checksum_type standard);

uint32_t CalculateDeltaT(uint32_t start, uint32_t end);

#endif /* INC_UTILS_H_ */
