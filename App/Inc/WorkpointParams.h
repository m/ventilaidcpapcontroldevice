/*
 * WorkpointSettings.h
 *
 *  Created on: 21 May 2020
 *      Author: Piotr
 */

#ifndef INC_WORKPOINTPARAMS_H_
#define INC_WORKPOINTPARAMS_H_

#include "main.h"

/* PROTOCOL VERSION V.2 */
// values that are loaded from memory on device init
float Workpoint_calibration_C0;
float Workpoint_calibration_C1;
float Workpoint_calibration_C2;
float Workpoint_calibration_C3;
float Workpoint_calibration_C4;

// values that are received from front panel and stored in memory
float Workpoint_calibration_set_C0;
float Workpoint_calibration_set_C1;
float Workpoint_calibration_set_C2;
float Workpoint_calibration_set_C3;
float Workpoint_calibration_set_C4;

void WorkpointParamsLoadFromEeprom(void);
void WorkpointParamsStoreReceivedParams(void);

#endif /* INC_WORKPOINTPARAMS_H_ */
