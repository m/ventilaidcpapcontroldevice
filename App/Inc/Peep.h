/*
 * Peep.h
 *
 *  Created on: 06.05.2020
 *      Author: Maciej
 */

#ifndef INC_PEEP_H_
#define INC_PEEP_H_

#include "main.h"

// TIM2
#define PEEP_OUT_MIN 0 // minimum signal output
#define PEEP_OUT_MAX 1000 // maximum signal otuput
#define PEEP_OUTPUT_CHANNEL TIM_CHANNEL_1

/*
 * Initialize GPIO port and sets output Off
 */
void PeepInit(void);

/*
 * Sets peep output
 * Arguments:
 * _output (1 - output on, 0 - output off)
 */
void PeepSetOutput(uint16_t _output);

#endif /* INC_PEEP_H_ */
