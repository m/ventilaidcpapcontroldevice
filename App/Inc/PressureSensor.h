/*
 * PressureSensor.h
 *
 *  Created on: 01.04.2020
 *      Author: Maciej
 */

#ifndef INC_PRESSURESENSOR_H_
#define INC_PRESSURESENSOR_H_

#include "main.h"

#define NUMBER_OF_PRESSURE_SENSORS 4
#define PressIntMulti 100 //Multiplier for pressure calculation

//variables for pressure calculation
#define PsiTocm 70.307

/* PROTOCOL VERSION V.2 */

// values that are loaded from memory on device init
float Pressure_Sensor_0_calibration_C0;
float Pressure_Sensor_0_calibration_C1;
float Pressure_Sensor_0_calibration_C2;
float Pressure_Sensor_0_calibration_C3;

float Pressure_Sensor_1_calibration_C0;
float Pressure_Sensor_1_calibration_C1;
float Pressure_Sensor_1_calibration_C2;
float Pressure_Sensor_1_calibration_C3;

float Pressure_Sensor_2_calibration_C0;
float Pressure_Sensor_2_calibration_C1;
float Pressure_Sensor_2_calibration_C2;
float Pressure_Sensor_2_calibration_C3;

float Pressure_Sensor_3_calibration_C0;
float Pressure_Sensor_3_calibration_C1;
float Pressure_Sensor_3_calibration_C2;
float Pressure_Sensor_3_calibration_C3;

float Pressure_Sensor_0;
float Pressure_Sensor_1;
float Pressure_Sensor_2;
float Pressure_Sensor_3;

// values that are received from front panel and stored in memory
float Pressure_Sensor_0_set_calibration_C0;
float Pressure_Sensor_0_set_calibration_C1;
float Pressure_Sensor_0_set_calibration_C2;
float Pressure_Sensor_0_set_calibration_C3;

float Pressure_Sensor_1_set_calibration_C0;
float Pressure_Sensor_1_set_calibration_C1;
float Pressure_Sensor_1_set_calibration_C2;
float Pressure_Sensor_1_set_calibration_C3;

float Pressure_Sensor_2_set_calibration_C0;
float Pressure_Sensor_2_set_calibration_C1;
float Pressure_Sensor_2_set_calibration_C2;
float Pressure_Sensor_2_set_calibration_C3;

float Pressure_Sensor_3_set_calibration_C0;
float Pressure_Sensor_3_set_calibration_C1;
float Pressure_Sensor_3_set_calibration_C2;
float Pressure_Sensor_3_set_calibration_C3;

typedef struct {
    float OutMax;
    float OutMin;
    float PreMax;
    float PreMin;
    float Multip; //=(PreMax - PreMin) / (OutMax - OutMin);
} PressParam;

PressParam PressureParameters[4];

uint16_t pressure_sensor_reading[4];
uint8_t pressure_sensor_status[4];

void PressureUpdate(void);

void PressureSensorInit(void);

void PressAverage(void);

/*
 * Update raw measurements of all sensors
 */
void PressureSensorUpdateMeasurements(void);

void PressureSensorParamsLoadFromEeprom(void);

void PressureSensor0ParamsStoreReceivedParams(void);
void PressureSensor1ParamsStoreReceivedParams(void);
void PressureSensor2ParamsStoreReceivedParams(void);
void PressureSensor3ParamsStoreReceivedParams(void);

/*
 * Function used to read pressure from _sensor_number
 */

#endif /* INC_PRESSURESENSOR_H_ */
