# VentilAid Control Device Software

Software for control device based on STM32F103RCT6. It uses CubeMX to generate inital code and than raw registers or HAL.
AtollicStudio is used as IDE.
In future we'll get rid of CubeMX generated code in favour of raw registers low level setup.  

# Naming convention

Each module should have it's own .h/.c files (ModuleName.h/.c - CamelCase),  
Variable name: modulename_variable_name (snake_case),  
Function name: ModuleNameFunctionName (CamelCase)  

# Hardware usage

ADC1 CH10 -> 5V rail voltage measurement (pin: PC0),  
ADC1 CH11 -> 12V rail voltage measurement (pin: PC1),  
ADC1 CH12 -> Potentiometer input voltage measurement (pin: PC2),   
USART2 -> Link to the Control Panel  (pins: PA2: TX, PA3: RX),  
DMA1 -> Transfer USART2 data from device to control panel, ADC measurements to array,  
SPI1 -> Connection to pressure sensors (up to 4, pins: PA6: MISO, PA7: MOSI, PA5: SCK, PB1: CS0, PB0: CS1, PC4: CS2, PC5: CS3),  
SPI2 -> External smart LED control (WS2812, pin: PB15: MOSI),  
TIM1 CH1 -> RCPWM output for brushless motor regulator (50Hz, pin: PA8),  
TIM1 CH2 -> RCPWM output for servo 0 (50Hz, pin: PA9),  
TIM1 CH3 -> RCPWM output for servo 1 (50Hz, pin: PA10),  
TIM2 CH1 -> PWM for PEEP valve (20kHz, pin: PA15),  
TIM3 -> Interrupt generation for VentilatorSpin function (frequency defined in main.h),  
TIM4 -> Interrupt generation for other system task (frequency defined in main.h),  
TIM8 CH1 -> Brushless motor RPM sensor (external clock mode, counts rising edged, pin: PC6),  
SWD -> Debug (pins: PA13: SWDIO, PA14: SWCLK),  
GPIO:  
PA0: Front panel GPIO0,  
PA1: Front panel GPIO1,  
PA11: Control built-in buzzer,  
PB3: Reset external alarm (missing pulse detector, min frequency 10Hz, should be controlled from main loop not interrupt!),
FLASH: page 124, 125, 126 and 127 emulates EEPROM,    

External clock: 8MHz, SYSCLK: 72MHz,  



