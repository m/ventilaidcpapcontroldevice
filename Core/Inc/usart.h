/*
 * usart.h
 *
 *  Created on: 01.04.2020
 *      Author: Maciej
 */

#ifndef INC_USART_H_
#define INC_USART_H_

#include "main.h"

#define USART2_RX_BUFFER_SIZE 500 //max 65536
uint8_t USART2_Rx_Buffer[USART2_RX_BUFFER_SIZE];

#define USART2_TX_BUFFER_SIZE 500
volatile uint8_t USART_Tx_Buffer[USART2_TX_BUFFER_SIZE];
volatile uint16_t USART_Tx_Buffer_head;
volatile uint16_t USART_Tx_Buffer_tail;
volatile uint16_t USART_Tx_Buffer_start_index;
volatile uint8_t USART_Tx_Buffer_users;
volatile uint16_t USART_Tx_Buffer_bytes_left;
volatile uint8_t USART2_Tx_In_Progress;

void USART2Init(void);
void USART2ConfigureTxDma(volatile uint8_t* _buffer, uint16_t _buffer_size);
void USART2ConfigureRxDma(void);
uint16_t USART2GetCurrentBufferHead(void);
uint8_t USART2RxBufferReadByte(uint8_t* buffer, uint16_t start_element, uint16_t element_number);
void USART2TxBufferWriteBytes(volatile uint8_t* output_buffer, uint8_t* input_buffer, uint8_t size);
void USART2TxBufferWriteByte(volatile uint8_t* buffer, uint16_t start_element, uint16_t element_number, uint16_t write_value);
uint16_t USART2RxBufferGetBytesInBuffer(uint16_t head, uint16_t tail);
uint16_t USART2TxBufferGetBytesInBuffer(uint16_t head, uint16_t tail);
void USART2PushDataOut(void);

#endif /* INC_USART_H_ */
