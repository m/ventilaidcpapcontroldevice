/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.h
 * @brief          : Header for main.c file.
 *                   This file contains the common defines of the application.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define VENTILATOR_SPIN_FREQUENCY 100
#define TASK_TIMER_FREQUENCY 2000
#define RASP_GPIO_1_Pin GPIO_PIN_0
#define RASP_GPIO_1_GPIO_Port GPIOA
#define RASP_GPIO_2_Pin GPIO_PIN_1
#define RASP_GPIO_2_GPIO_Port GPIOA
#define SPI1_EX_CS2_Pin GPIO_PIN_4
#define SPI1_EX_CS2_GPIO_Port GPIOA
#define SPI1_CS3_Pin GPIO_PIN_4
#define SPI1_CS3_GPIO_Port GPIOC
#define SPI1_CS4_Pin GPIO_PIN_5
#define SPI1_CS4_GPIO_Port GPIOC
#define SPI1_CS2_Pin GPIO_PIN_0
#define SPI1_CS2_GPIO_Port GPIOB
#define SPI1_CS1_Pin GPIO_PIN_1
#define SPI1_CS1_GPIO_Port GPIOB
#define SPI1_EX_CS1_Pin GPIO_PIN_2
#define SPI1_EX_CS1_GPIO_Port GPIOB
#define RCPWM_OUT_Pin GPIO_PIN_8
#define RCPWM_OUT_GPIO_Port GPIOA
#define SERVO0_Pin GPIO_PIN_9
#define SERVO0_GPIO_Port GPIOA
#define SERVO1_Pin GPIO_PIN_10
#define SERVO1_GPIO_Port GPIOA
#define BUZZER_Pin GPIO_PIN_11
#define BUZZER_GPIO_Port GPIOA
#define PWR_VALID_Pin GPIO_PIN_12
#define PWR_VALID_GPIO_Port GPIOA
#define PEEP_Pin GPIO_PIN_15
#define PEEP_GPIO_Port GPIOA
#define EXTERNAL_ALARM_Pin GPIO_PIN_3
#define EXTERNAL_ALARM_GPIO_Port GPIOB
#define ENC1_Pin GPIO_PIN_4
#define ENC1_GPIO_Port GPIOB
#define ENC2_Pin GPIO_PIN_5
#define ENC2_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

#define dOther 9
#define dInhale 0
#define dPostInhale 1
#define dExhale 2
#define dPostExhale 3
#define dTalk 4
#define dCough 5
#define dLaught 6
#define dLeak 7
#define dSamplesPerMinute (60 * VENTILATOR_SPIN_FREQUENCY)
#define MAIN_CLOCK 72000000UL
#define USART2_BAUD_RATE 460800UL
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
