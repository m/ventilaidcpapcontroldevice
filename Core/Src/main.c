/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/

#include "main.h"
#include "adc.h"
#include "dma.h"
#include "gpio.h"
#include "spi.h"
#include "tim.h"
#include <PidController.h>
#include <RpmSensor.h>

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "Alarm.h"
#include "App.h"
#include "Buzzer.h"
#include "Eeprom.h"
#include "EepromMemoryMap.h"
#include "FlowParams.h"
#include "LED.h"
#include "PIDParams.h"
#include "Peep.h"
#include "PressureSensor.h"
#include "RcPwmOutput.h"
#include "Utils.h"
#include "Ventilator.h"
#include "WorkpointParams.h"
#include "usart.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
uint8_t InitDone = 0;
uint32_t last_send_time, check_message_last_spin_time;
uint32_t last_uart_send_time;
float input_test;
uint8_t array_test[4];
PIDControl pid_structure;
PIDControl* pid = &pid_structure;

float pid_kpb = 60;
float pid_kib = 0.1;
float pid_kdb = 50;

float pid_kpn = 40;
float pid_kin = 0.5;
float pid_kdn = 100;

float pid_kpa = 60;
float pid_kia = 0.1;
float pid_kda = 50;

float pid_sampleTimeSeconds = 1 / VENTILATOR_SPIN_FREQUENCY;
float pid_minOutput = 150;
float pid_maxOutput = 1000;

PIDControl PeepPid_structure;
PIDControl* PeepPid = &PeepPid_structure;
float PeepPid_kp = 0; //80;
float PeepPid_ki = 0; //7;//bylo 9
float PeepPid_kd = 0; //100;// bylo 1
float PeepPid_sampleTimeSeconds = 1 / VENTILATOR_SPIN_FREQUENCY;
float PeepPid_minOutput = 0;
float PeepPid_maxOutput = 1000;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
    /* USER CODE BEGIN 1 */
    AlarmInit();
    /* USER CODE END 1 */

    /* MCU Configuration--------------------------------------------------------*/

    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    /* USER CODE BEGIN Init */

    /* USER CODE END Init */

    /* Configure the system clock */
    SystemClock_Config();

    /* USER CODE BEGIN SysInit */

    /* USER CODE END SysInit */

    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_DMA_Init();
    MX_ADC1_Init();
    MX_SPI1_Init();
    MX_TIM1_Init();
    MX_TIM2_Init();
    MX_TIM3_Init();
    MX_TIM4_Init();
    MX_SPI2_Init();
    /* USER CODE BEGIN 2 */
    check_message_last_spin_time = 0;
    LedSetRgb(255, 255, 0); //orange led - initializing

    //init EEPROM emulation
    HAL_FLASH_Unlock();
    EepromInit();

    //load variables from memory
    WorkpointParamsLoadFromEeprom();
    FlowParamsLoadFromEeprom();
    PIDParamsLoadFromEeprom();
    PressureSensorParamsLoadFromEeprom();

    //init PEEP output
    PeepInit();

    //init buzzer output
    BuzzerInit();

    //init app
    AppInit();

    //init communication with front panel
    USART2Init();

    //init PWM outputs to motor and servos
    RcPwmOutputInit();

    //init pressure sensors
    PressureSensorInit();

    //init motor rpm sensor
    RPMSensorInit();

    //init timers
    HAL_TIM_Base_Start_IT(&htim3);
    HAL_TIM_Base_Start_IT(&htim4);

    //init adc in dma mode
    HAL_ADC_Start_DMA(&hadc1, (uint32_t*)adc_values, 4);

    //init Ventilator
    VentilatorInit();
    BuzzerSetOutput(0);

    //set peep output to default value
    PeepSetOutput(500);
    RcPwmMotorSetOutput(0);

    //wait for ESC to initalize itself (it needs 1000us signal for at least 8 sec)
    HAL_Delay(10000);

    //start motor
    RcPwmMotorSetOutput(75);
    HAL_Delay(500);
    RcPwmMotorSetOutput(125);
    HAL_Delay(500);
    RcPwmMotorSetOutput(175);

    //init PIDs
    PIDInit(pid, pid_kpn, pid_kin, pid_kdn, pid_sampleTimeSeconds, pid_minOutput, pid_maxOutput, AUTOMATIC, DIRECT);
    PIDTuningsGroupsSet(pid, pid_kpb, pid_kib, pid_kdb, pid_kpn, pid_kin, pid_kdn, pid_kpa, pid_kia, pid_kda);

    PIDInit(PeepPid, PeepPid_kp, PeepPid_ki, PeepPid_kd, PeepPid_sampleTimeSeconds, PeepPid_minOutput, PeepPid_maxOutput, AUTOMATIC, REVERSE);
    PeepPid->MaxSample = 16;

    LedSetRgb(0, 255, 0); //green led - everything initialized
    InitDone = 1;
    /* USER CODE END 2 */

    /* Infinite loop */
    /* USER CODE BEGIN WHILE */
    while (1) {
        //AlarmCheckAlarmsAndUpdateDeviceStatus(); // Check alarms and update status bits
        uint32_t current_time = HAL_GetTick();
        if (current_time - last_send_time >= (1000 / app_data_send_rate)) {

            AppSendMainParams();
            AppSendDebugParams();

            last_send_time = current_time;
        }

        //temporary solution, will be handled by timer
        if (current_time - last_uart_send_time >= (1000 / uart_send_rate)) {
            USART2PushDataOut();
            last_uart_send_time = current_time;
        }
        /* USER CODE END WHILE */

        /* USER CODE BEGIN 3 */
    }
    /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
    RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
    RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };
    RCC_PeriphCLKInitTypeDef PeriphClkInit = { 0 };

    /** Initializes the CPU, AHB and APB busses clocks 
  */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
        Error_Handler();
    }
    /** Initializes the CPU, AHB and APB busses clocks 
  */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
        | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK) {
        Error_Handler();
    }
    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
    PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK) {
        Error_Handler();
    }
}

/* USER CODE BEGIN 4 */
__attribute__((interrupt)) void DMA1_Channel7_IRQHandler(void)
{
    //USART2 TX DMA Channel
    //interrupt is generated when transmission error occurs
    //watch for USART TC flag to see if actual transmission is complete!
    if (DMA1->ISR & DMA_ISR_TEIF7) {
        DMA1->IFCR = DMA_IFCR_CTEIF7;
        //transmission error, we need to handle it
        //TODO
    }
}

__attribute__((interrupt)) void DMA1_Channel6_IRQHandler(void)
{
    //USART2 RX DMA Channel
    //interrupt is generated when transmission error occurs
    if (DMA1->ISR & DMA_ISR_TEIF6) {
        DMA1->IFCR = DMA_IFCR_CTEIF6;
        //transmission error, we need to handle it
        //TODO
    }
}

__attribute__((interrupt)) void USART2_IRQHandler(void)
{
    uint32_t __attribute__((unused)) dummy_word;
    if (USART2->SR & USART_SR_RXNE) {
        USART2->SR &= ~USART_SR_RXNE;
        __attribute__((unused)) uint8_t tmp;
        tmp = (uint8_t)USART2->DR;
    }
    if (USART2->SR & USART_SR_TC) {
        //transfer complete
        USART2->SR &= ~USART_SR_TC;
        if (USART2_Tx_In_Progress) {
            USART2_Tx_In_Progress = 0;
            if (USART_Tx_Buffer_bytes_left) {
                USART_Tx_Buffer_head = 0;
                USART2ConfigureTxDma(USART_Tx_Buffer, USART_Tx_Buffer_bytes_left);
                USART_Tx_Buffer_bytes_left = 0;
            } else {
                USART_Tx_Buffer_head = USART_Tx_Buffer_tail;
            }
        }
    }

    if (USART2->SR & USART_SR_FE) {
        //framing error!
        //sequence to clear that error:
        dummy_word = USART2->SR;
        dummy_word = USART2->DR;
    }

    if (USART2->SR & USART_SR_ORE) {
        //overrun error!
        //sequence to clear that error:
        dummy_word = USART2->SR;
        dummy_word = USART2->DR;
    }

    if (USART2->SR & USART_SR_NE) {
        //noise error!
        //sequence to clear that error:
        dummy_word = USART2->SR;
        dummy_word = USART2->DR;
    }
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim)
{
    if (htim->Instance == htim3.Instance) {
        VentilatorSpin();
        AppCheckForNewIncomingMessage();
    }
    if (htim->Instance == htim4.Instance) {

        PressureSensorUpdateMeasurements();
        RPMSensorUpdate();
    }
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
    /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */

    /* USER CODE END Error_Handler_Debug */
}

#ifdef USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* USER CODE BEGIN 6 */
    /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
    /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
